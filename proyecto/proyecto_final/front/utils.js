import jwt from 'jwt-decode'
import axios from 'axios'

export async function login( email, contrasena ) {

    try {

        await axios.post('http://localhost:8000/auth', {
            email: email,
            contrasena: contrasena
        })
        
        
        .then(function(response){

            // ME GUARDO EL TOKEN
            setAuthToken(response.data.token)

            // MEGUARDO EL NOMBRE DE EMAIL
            setEmail(response.data.email)

            // MEGUARDO EL ID DE USUARIO
            setId(response.data.id)

        })

    } catch(error) {
        console.log(error)
    }

}

// FUNCION PARA GUARDAR EN LOCALSTORAGE EL JSONWEBTOKEN
export function setAuthToken(token) {
    axios.defaults.headers.common['Athorization'] = `Bearer ${token}`
    localStorage.setItem('AUTH_TOKEN_KEY', token)
}

// FUNCION PARA RECUPERAR EL TOKEN DESDE LOCALSTORAGE
export function getAuthToken() {
    return localStorage.getItem('AUTH_TOKEN_KEY')
}

// FUNCION PARA CONSEGUIR LA FECHA DE CADUCIDAD DEL TOKEN
export function tokenExpiration(encodedToken){
    let token = jwt(encodedToken)

    if (!token.exp){
        return null
    }

    let date = new Date(0)
    date.setUTCSeconds(token.exp)

    return date
}

// FUNCION QUE COMPRUEBA SI EL TOKEN ESTA CADUCADO
export function isExpired(token){
    let expirationDate = tokenExpiration(token)
    return expirationDate < new Date()
}

// FUNCION QUE COMPRUEBA SI OTRA PERSONA ESTA LOGEADA Y EL TOKEN ES VALIDO
export function isLoggedIn(){
    let authToken = getAuthToken()
    return !!authToken && !isExpired(authToken)
}

// FUNCION DE GUARDAR EL NOMBRE DE EMAIL EN EL LOCALSTORAGE
export function setEmail(email){
    localStorage.setItem('EMAIL', email)
}

// FUNCION DE GUARDAR EL ID EN EL LOCALSTORAGE
export function setId(id){
    localStorage.setItem('ID', id)
}

// FUNCION PARA RECUPERAR EL NOMBRE DE EMAIL EN LOCALSTORAGE
export function getEmail(){
    return localStorage.getItem('EMAIL')
}

// FUNCION PARA RECUPERAR EL ID EN LOCALSTORAGE
export function getId(){
    return localStorage.getItem('ID')
}

// FUNCION DE LOGOUT
export function logout(){
    axios.defaults.headers.common['Athorization'] = ''
    localStorage.removeItem('AUTH_TOKEN_KEY')
    localStorage.removeItem('EMAIL')
    localStorage.removeItem('ID')
}
