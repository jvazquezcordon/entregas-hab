import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import { isLoggedIn } from '../../utils.js'

Vue.use(VueRouter)

  const routes = [

    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/products',
      name: 'productos',
      component: () => import('../views/Products.vue')
    },
    {
      path: '/new-user',
      name: 'micuenta',
      component: () => import('../views/MiCuenta.vue')
    },
    {
      path: '/user/login',
      name: 'login',
      component: () => import('../views/Login.vue')
    },
    {
      path: '/user',
      name: 'nuevousuario',
      component: () => import('../views/NuevoUsuario.vue'),
      meta: {
        sinLoggin: true
      }
    },
    {
      path: '/añadirproducto',
      name: 'nuevoproducto',
      component: () => import('../views/NuevoProducto.vue')
    },
    {
      path: '/rating',
      name: 'rating',
      component: () => import('../views/Rating.vue')
    }
  
  ]

const router = new VueRouter({
  routes
})

router.beforeEach( ( to, from, next) => {
  if( to.meta.sinLoggin === true && isLoggedIn() === true ){
        next({
          path:'/',
          query: {redirect: to.fullPath}
        })
  } else {
    next()
  }
})

export default router
