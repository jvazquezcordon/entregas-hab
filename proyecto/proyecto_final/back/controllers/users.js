require('dotenv').config();

const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");
const express = require('express');
const fileUpload = require('express-fileupload');
const moment = require('moment');
const app = express();
const bd = require('./bd_mock');
const llave = process.env.llave;

app.use(fileUpload());

// -> Consulta usuario por ID
const userSQL = async ( req, res) => {

    const id = req.params.id

    try {
        const getUser = await bd.getUserSQL(id)
        res.status(200).json(getUser);
    } catch (error) {
        console.log(error)
    }
    
}

// -> Consulta usuarios
const usersSQL = async ( req, res) => {

    const getUser = await bd.getUsersSQL()
    res.status(200).json(getUser);
}

// -> Registro usuario nuevo
const registerUserSQL = async ( req, res) => {

    const { nombre, apellidos, direccion, email, contrasena, foto } = req.body;
    const userExist = await bd.userExistSQL(email);

    if (userExist[0].length > 0) {
        console.log('Este usuario ya existe')
        res.status(400).json('Este email ya existe');   
    } else {
        await bd.registerSQL(nombre, apellidos, direccion, email, contrasena, foto)
        res.status(200).json('Usuario registrado ¡¡¡');
    }

    let transporter = await nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 25,
        secure: false,
        auth: {
            user: process.env.EMAIL,
            pass: process.env.CLAVE
        }
        });

        transporter.sendMail({
        from: 'pruebamailparser@gmail.com',
        to: email,
        subject: "Usuario registrado",
        text: 'Te has registrado en la tienda, gracias',
        html: `<h1>Usuario Registrado ¡¡¡¡¡</h1>`

        });
}

// -> Login de usuario
const loginSQL = async (req, res) => {

    const { email, contrasena } = req.body;

    console.log(email);
    console.log(contrasena);

    if ( email === undefined ||
        contrasena === undefined ){
        res.status(400).json('Faltam parámetros que rellenar')
        return;
    }

    const userId = await bd.getUserIdSQL(email);
    const userIdMap = userId.map( user => user['id']);

    const tokenPayload = {
        email,
        userIdMap
    };

    const token = jwt.sign(tokenPayload, process.env.SECRET, {
        expiresIn: '10d'
    });

    const responseDTO = await bd.loginUserSQL(email, contrasena);
    
    if ( responseDTO['description'].indexOf('incorrectos') !== -1 ){
        res.status(404).json('Usuario/password incorrectos')
        return
    }

    res.status(200).json({Token: token});
};

// -> Cambiar campos de usuario
const editarUsuarioSQL = async (req, res) => {

    let { id } = req.query;
          id = parseInt(id);
    
    const { nombre, apellidos, direccion, email, foto} = req.body    

    let verify = await bd.getUserSQL(idUser);

    if (verify === undefined) {
        res.send('Usuario no encontrado');
        return;
    }

    if ( usuario !== undefined ){

        const edicionUser = await bd.editUserSQL(nombre, apellidos, direccion, email, foto, id);
        console.log(edicionUser)
    }

}

const editarContraseñaSQL = async (req, res) => {


        const { contrasena } = req.body;

        let { id } = req.query;
              id = parseInt(id);

        nuevoPassword = await bd.editContraseñaSQL(contrasena, id);
        res.json('Usuario y contraseña modificadas');

}


// -> Envia rating de cada usuario chequeando cada 24 horas 
const checkRatingSQL = async () => {

    let fechaActual = moment().format('llll');
        fechaActual = fechaActual.split(',');
        fechaActual = fechaActual[1];

    let reservas = await bd.getReservasSQL();
        reservas = Object.values(reservas[0]);

    let fechasRating = reservas.map(reserva => reserva['fecha_rating']);
        fechasRating = fechasRating.map(reserva => reserva.toString());

    const mapFechasRating =  fechasRating.map(fechas => fechas.includes(fechaActual));
    const stringFechasRating = mapFechasRating.map(reserva => reserva.toString());

    let indicesFechas = [];
    let elemento = 'true';
    let posicion = stringFechasRating.indexOf(elemento);

    while (posicion != -1) {
        indicesFechas.push(posicion);
        posicion = stringFechasRating.indexOf(elemento, posicion + 1);
    }       

    let emailUsers = await bd.getUsersSQL();
        emailUsers = Object.values(emailUsers[0])
        emailUser = emailUsers.map(user => user['email']);

    // SE PRESUPONE QUE NO SE PUEDEN ELIMINAR NI USUARIOS NI RESERVAS PARA QUE COINCIDA EL INDICE CON EL ID

    const mapIndicesEmail = indicesFechas.map( indices => {

        let id = indices;
            id = emailUser[id];

        return id
    })

    const idReservas = reservas.map( reserva => reserva[id])

    if ( mapIndicesEmail.length !== 0 ){

        let transporter = await nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 25,
        secure: false,
        auth: {
            user: process.env.EMAIL,
            pass: process.env.CLAVE
        }
        });

        transporter.sendMail({
        from: 'pruebamailparser@gmail.com',
        to: mapIndicesEmail.toString(),
        subject: "Queremos saber tu opinion",
        text: 'hola',
        html: `<div>Rating</div>
                <a>http:localhost:8000/rating?idUser=${id}&idReserva=${idReservas}</a>` // TODO --> INCLUIR URL CON LOS VALORES ID_USER E ID_RESERVA

        });
    }

}

// -> Envia mail de rating a cada usuario y asigna ventas y rating en el campo user
const nuevoRatingSQL = async  (req, res) => {

    let { idUser, idReserva } = req.query;
          idUser = parseInt(idUser);

    let { rating } = req.body;  
        rating = parseInt(rating)

    try {

        let idUserReservaOk = await bd.getIdUserReserva(idReserva);
            idUserReservaOk = Object.values(idUserReservaOk[0])[0]['id_user'];
            console.log(idUserReservaOk)

        if ( idUser === idUserReservaOk ){
            const ratingAnterior = await bd.getRatingSQL(idUser)
            console.log(ratingAnterior[0])
            let ratingMap = ratingAnterior[0].map( rating => parseInt(rating.rating))
                ratingMap = ratingMap.push(rating);
 
            ratingMedia = (rating+ratingMap)/2
    
            const ratingUser = await bd.nuevoRatingSQL(ratingMedia, idUser);
            res.status(200).json(ratingUser);

        }else {
            console.log('Datos incorrectos')
        }
    
    } catch (e) {
        res.status(400).send('Error en nuevo rating');
        return   
    }
    
}

const isUserLogged = async  (req, res) => {
    
    const email = req.body.email
    const contrasena = req.body.contrasena

    try {
        let userExist = await bd.loginUserSQL(email, contrasena)
        userExist = Object.values(userExist[0])
        console.log(userExist)

        if(userExist.length > 0){

            const payload = {
                check: true
            }

            // GUARDANDO EL EMAIL
            let email = ''
            email = userExist[0].email

            // GUARDANDO EL ID
            let id = ''
            id = userExist[0].id

            // TOKEN
            const token = jwt.sign(payload, llave, {
                expiresIn: '2 days'
            })

            /*res.status(200).*/res.json({
                mensaje: 'Atentificación ok¡',
                token: token,
                email: email,
                id: id
            })
        } else {
            res.status(400)
            console.log('Datos incorrectos')
        }
        
    } catch (error) {
        console.log(error)
    }

}

module.exports = {
    userSQL,
    usersSQL,
    loginSQL,
    registerUserSQL,
    editarUsuarioSQL,
    editarContraseñaSQL,
    checkRatingSQL,
    nuevoRatingSQL,
    isUserLogged
}