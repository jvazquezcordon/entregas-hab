const database = require('../database');

async function getProductosSQL () {
    const sql = 'SELECT * FROM producto';
    const conection = await database.connection();
    const rows = await conection.execute(sql);
    return rows;
}

async function getProductoSQL (id) {
    const sql = 'SELECT * FROM producto WHERE id = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [id]);
    return rows;
}

async function borrarProducto(id){
    const sql = 'DELETE FROM producto WHERE id = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [id]);
    return rows;
}

async function checkProductoUbicacionSQL (ubicacion) {
    const sql = 'SELECT * FROM producto WHERE ubicacion = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [ubicacion]);
    return rows;
}

async function checkProductoPrecioSQL (precio) {
    const sql = 'SELECT * FROM producto WHERE precio = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [precio]);
    return rows;
}

async function nuevoProductoSQL (categoria, descripcion, ubicacion, precio, email, horario_lugar, id_user, imagen) {
    const sql = 'INSERT INTO producto (categoria, descripcion, ubicacion, precio, email, horario_lugar, id_user, imagen) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [categoria, descripcion, ubicacion, precio, email, horario_lugar, id_user, imagen]);
    return rows;
}

async function productoEstadoReservadoSQL (reservado, idProducto) {
    const sql = 'UPDATE reserva SET estado = ? WHERE id_Producto = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [reservado, idProducto]);
    return rows;
}

async function checkCategoriaSQL (category) {
    const sql = 'SELECT * FROM producto WHERE categoria = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [category]);
    return rows;
}

async function userExistSQL(email) {
    const sql = 'SELECT id FROM user WHERE email = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [email]);
    return rows;
}

async function registerSQL(nombre, apellidos, direccion, email, contrasena, foto) {

    try {
        const sql = 'INSERT INTO user (nombre, apellidos, direccion, email, contrasena, foto) VALUES (?, ?, ?, ?, SHA2(?, 512), ?)';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [nombre, apellidos, direccion, email, contrasena, foto]);
    return rows;
    } catch (error) {
        const responseDTO = {'code': 500,
                            'description': error};
        return responseDTO;
    }
}

async function loginUserSQL(email, contrasena) {
    
    const sql = 'SELECT * FROM user WHERE email = ? AND contrasena = SHA2(?, 512)';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [email, contrasena]);
    return rows;
}

async function getUsersSQL () {
    const sql = 'SELECT * FROM user';
    const conection = await database.connection();
    const rows = await conection.execute(sql);
    return rows;
}

async function getUserSQL (id) {
    const sql = 'SELECT nombre, apellidos, direccion, email, foto FROM user WHERE id=?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [id]);
    return rows;
}

async function getUserIdSQL(email) {

    const sql = 'SELECT * FROM user WHERE email = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [email]);
    return rows;
}

async function getIdUserReserva(id) {

    const sql = 'SELECT id_user FROM reserva WHERE id = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [id]);
    return rows;
}

async function editUserSQL (nombre, apellidos, direccion, email, foto, id) {

    const sql = 'UPDATE user SET nombre = ?, apellidos = ?, direccion = ?, email = ?, foto = ? WHERE id = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [nombre, apellidos, direccion, email, foto, id]);
    return rows;

}

async function editContraseñaSQL (contrasena, id) {

    const sql = 'UPDATE user SET contrasena = SHA2(?, 512) WHERE id = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [contrasena, id]);
    return rows;
}

async function nuevaReservaSQL (fecha, fecha_rating, estado, codigo_reserva, observaciones, precio_final, id_user, id_producto) {
    
    try {
        const sql = 'INSERT INTO reserva (fecha, fecha_rating, estado, codigo_reserva, observaciones, precio_final, id_user, id_producto) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
        const conection = await database.connection();
        const rows = await conection.execute(sql, [fecha, fecha_rating, estado, codigo_reserva, observaciones, precio_final, id_user, id_producto]);
        return rows;
    } catch (error) {
        console.log('Error en MySql')
    }
    
}

async function borrarProductoSQL (id) {
    
    try {
        const sql = 'DELETE FROM producto WHERE id = ?';
        const conection = await database.connection();
        const rows = await conection.execute(sql, [id]);
        return rows;
    } catch (error) {
        console.log(error)
    }
    
}

async function getReservasSQL () {
    const sql = 'SELECT * FROM reserva';
    const conection = await database.connection();
    const rows = await conection.execute(sql);
    return rows;
}

async function getReservasIdProductoSQL () {
    const sql = 'SELECT id_producto FROM reserva';
    const conection = await database.connection();
    const rows = await conection.execute(sql);
    return rows;
}

async function getRatingSQL(id) {

    const sql = 'SELECT rating FROM user WHERE id = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [id]);
    return rows;
}

async function nuevoRatingSQL(rating, id) {

    const sql = 'UPDATE user SET rating = ? WHERE id = ?';
    const conection = await database.connection();
    const rows = await conection.execute(sql, [rating, id]);
    return rows;
}

module.exports = {
    getProductosSQL,
    getProductoSQL,
    borrarProducto,
    checkProductoUbicacionSQL,
    checkProductoPrecioSQL,
    nuevoProductoSQL,
    productoEstadoReservadoSQL,    
    checkCategoriaSQL,
    userExistSQL,
    registerSQL,
    loginUserSQL,
    getUsersSQL,
    getUserSQL,    
    getUserIdSQL,
    getIdUserReserva,
    editUserSQL,
    editContraseñaSQL,
    nuevaReservaSQL,
    borrarProductoSQL,
    getReservasSQL,
    getReservasIdProductoSQL,
    getRatingSQL,
    nuevoRatingSQL,
}