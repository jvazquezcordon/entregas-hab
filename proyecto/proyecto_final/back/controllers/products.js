const bd = require('./bd_mock');
const moment = require('moment');
const nodemailer = require("nodemailer");

// -> Lista todos los producto o filtra por categoria, ubicacion o precio
const listProductosSQL = async ( req, res) => {

    let {ubicacion} = req.query;

    if ( ubicacion !== undefined ) {
        let listaProductosBusqueda = await bd.checkProductoUbicacionSQL(ubicacion);
        return res.status(200).json(listaProductosBusqueda[0]);
    }

    let {precio} = req.query;

    if ( precio !== undefined ) {
        let listaProductosBusqueda = await bd.checkProductoPrecioSQL(precio);
        return res.status(200).json(listaProductosBusqueda[0]);
    }

    let {categoria} = req.query;

    if ( categoria !== undefined ) {
        let listaProductosBusqueda = await bd.checkCategoriaSQL(categoria);
        return res.status(200).json(listaProductosBusqueda[0]);
    }

    const listaProductos = await bd.getProductosSQL();
    
    return res.status(200).json(listaProductos);
}

// -> Añade producto 
const addSQL = async (req, res) => {

    let { id_user } = req.query;

    let {categoria, descripcion, ubicacion, precio, email, horario_lugar, imagen} = req.body

    if (categoria === undefined || categoria === '' ||
        descripcion === undefined || descripcion === '' ||
        ubicacion === undefined || ubicacion === '' ||
        precio === undefined || precio === '' ||
        email === undefined || email === '' ||
        horario_lugar === undefined || horario_lugar === '' ||
        imagen === undefined || imagen === '') {

        res.status(400).send('Campos por rellenar en el formulario');
        return;
    }

    if (categoria !== undefined || 
        descripcion !== undefined || 
        ubicacion !== undefined || 
        precio !== undefined || 
        email !== undefined || 
        horario_lugar !== undefined ||
        imagen !== undefined ) {

            categoria = categoria.toLowerCase();
            descripcion = descripcion.toLowerCase();
            ubicacion = ubicacion.toLowerCase();
            precio = precio.toLowerCase();
            email = email.toLowerCase();
            horario_lugar = horario_lugar.toLowerCase();
            //id_user = id_user;
            imagen = imagen.toLowerCase();

            await bd.nuevoProductoSQL(categoria, descripcion, ubicacion, precio, email, horario_lugar, id_user, imagen);
            res.json('Producto añadido');
        }

}

// -> Realiza la reserva y elimina stock de producto
const reservaSQL = async (req, res) => {
    
    try {

        let idProducto = req.query.idProducto 
        const userId = req.query.userId
        const email = req.query.email 

        let producto = await bd.getProductoSQL(idProducto);
            producto = producto[0];
    
        if( producto.length === 0 ){
            res.status(400).json('Id de producto no válida');
            return;
        }
    
        const fechaHoraCompra = moment().format('YY-MM-DD');
        const fechaRating = moment().add(10, 'days').calendar();
        const fechaRatingSplit = fechaRating.split('/');
        const fechaRatingAño = fechaRatingSplit[2];
        const fechaRatingSlice = fechaRatingAño.slice(2);
        const arrayFecha = [  fechaRatingSlice, fechaRatingSplit[0], fechaRatingSplit[1]]
        const arrayFechaRatingOk = arrayFecha.join('-');

        const codigoReserva = Math.random().toString().slice(12, -1);
            
        const fecha = fechaHoraCompra
        const fecha_rating = arrayFechaRatingOk
        const estado = ''
        const codigo_reserva = codigoReserva
        const observaciones = ''
        const precio_final = producto[0]['precio']

        let id_user = userId
            id_user = parseInt(id_user)
            id_producto = producto[0].id

        await bd.nuevaReservaSQL(fecha, fecha_rating, estado, codigo_reserva, observaciones, precio_final, id_user, id_producto);
    
        let transporter = await nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 25,
            secure: false,
            auth: {
                user: process.env.EMAIL,
                pass: process.env.CLAVE
            }
        });

        await transporter.sendMail({
            from: 'pruebamailparser@gmail.com',
            to: email,
            subject: "Gracias por tu reserva",
            text: idProducto,
            html: `<div>Código de la reserva: ${codigoReserva}</div>`

        });
    
    } catch (e) {
        return res.status(405).json('Mail no enviado a user en reserva');
    }
    
    res.status(200).json('Reserva realizada');

};

module.exports = {
    listProductosSQL,
    addSQL,
    reservaSQL
};