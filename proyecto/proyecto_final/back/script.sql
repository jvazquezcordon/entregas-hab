-- Script SQL

-- user ( #id, nombre, apellidos, direccion , email, contraseña, rating )
-- reserva ( #id, producto, email_comprador, email_vendedor, fecha, fecha_rating)
-- producto ( #id, categoria, descripcion, ubicacion, precio, email, horario_lugar)

USE proyecto;

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE user (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    direccion VARCHAR(50),
    email VARCHAR(50) UNIQUE NOT NULL,
    contrasena VARCHAR(40) NOT NULL,
    rating  VARCHAR(50)
);

CREATE TABLE reserva (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    fecha DATE,
    fecha_rating DATE,
    estado VARCHAR(50),
    codigo_reserva INT UNSIGNED AUTO_INCREMENT,
    observaciones VARCHAR(200),
    precio_final DECIMAL(7, 2) DEFAULT 0,
    id_user INT UNSIGNED,
    FOREIGN KEY (id_user) REFERENCES user(id),
    id_producto INT UNSIGNED,
    FOREIGN KEY (id_producto) REFERENCES producto(id)

);

CREATE TABLE producto (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    categoria VARCHAR(50),
    descripcion VARCHAR(150),
    ubicacion VARCHAR(50),
    precio DECIMAL(7, 2),
    email VARCHAR(50) NOT NULL,
    horario_lugar VARCHAR(50),
    id_user INT UNSIGNED,
    FOREIGN KEY (id_user) REFERENCES user(id)
);

SET FOREIGN_KEY_CHECKS = 1;
