require('dotenv').config();

// LIBRERIAS NPM
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
const app = express();

// LIBRERIAS PROPIAS
const { loginSQL, registerUserSQL, editarUsuarioSQL, editarContraseñaSQL, subirImagen, checkRatingSQL, nuevoRatingSQL, userSQL, usersSQL, isUserLogged } = require('./controllers/users');
// const { isAuthenticated } = require('./middlewares/auth_y_reserva.js')
const { addSQL, listProductosSQL, reservaSQL } = require('./controllers/products');
const port = process.env.PORT;

// CHECK MAIL RATING
setInterval(checkRatingSQL, 86400000); // 24 HORAS = 86400000

// APP USE
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

// APP GET
app.get('/products', listProductosSQL); //-> Listado de productos y filtro por categoria, ubicacion o precio
app.get('/user/:id', userSQL) //-> Consulta info del cliente por ID

// APP POST
app.post('/user', registerUserSQL); //-> Resgistro de usuario con envio de email al cliente ya integrado
app.post('/user/login', loginSQL); //-> Login de usuario 
app.post('/products', /*isAuthenticated,*/ addSQL); //-> Añadir producto a la tienda
app.post('/reserva', /*isAuthenticated,*/ reservaSQL); //-> Genera la reserva articulo(vendedor), genera compras (comprador) y elimina producto (array productos).
app.post('/rating', /*isAuthenticated,*/ nuevoRatingSQL); //-> Añade rating al perfil de usuario del vendedor
app.post('/auth', isUserLogged);

//APP PUT
app.put('/user/login', /*isAuthenticated,*/ editarUsuarioSQL); //-> Editar campo de usuario
app.put('/user/password', /*isAuthenticated,*/ editarContraseñaSQL); //-> Editar contraseña de usuario

// APP LISTEN
app.listen(port, () => {
});

app.listen(4000, async () => {
});
