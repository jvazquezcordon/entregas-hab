const mysql = require('mysql2/promise');

async function connection() {
    return await mysql.createConnection({
        host: 'localhost',
        user: 'jero',
        password: '123456',
        database: 'proyecto'
    });
}

module.exports = {
    connection
};