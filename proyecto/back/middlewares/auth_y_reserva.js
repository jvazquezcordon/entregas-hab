require('dotenv').config();
const jwt = require('jsonwebtoken');


// -> Permite autentificación del cliente, envía token por header
const isAuthenticated = (req, res, next) => {

    const {
        authorization
    } = req.headers;

    try {
        const decodedToken = jwt.verify(authorization, process.env.SECRET);
        req.auth = decodedToken;

    } catch (e) {
        const authError = new Error('Token no válido');
        authError.status = 401;
        return next(authError);
    }

    next();
}

module.exports = {
    isAuthenticated,
};