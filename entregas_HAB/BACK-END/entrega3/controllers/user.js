const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const bd = require('./bd_mock');

const login = async (req, res) => {

    const { userBody, password } = req.body;

    const user = bd.getUser().toLowerCase();
    const userPassword = process.env.userPassword;
    const passwordBcrypt = await bcrypt.hash(userPassword, 10);
    
    if (!userBody || !password) {
        res.status(404).send();
        return;
    }

    if ( user !== userBody.toLowerCase() ){
        res.status(404).send();
        return;
    }

    const passwordIsvalid = await bcrypt.compare(password, passwordBcrypt);

    if (!passwordIsvalid) {
        res.status(401).send();
        return;
    }

    const tokenPayload = { user };
    const token = jwt.sign(tokenPayload, process.env.SECRET, {
        expiresIn: '3d'
    });

    res.json({
        token
    })
}

module.exports = {
    login
}
