let events = [];
Id = 0;

const add = (req, res) => {

    const name = req.body.name;
    const stock = req.body.stock;
    const precio = req.body.precio;

    if (!req.auth || req.auth.user !== 'jero') {
        res.status(403).send();
        return;
    }

    if ( !name || !stock || !precio ) {
        res.status(400).send();
        return;
    }

    events.push({
        'id': Id++,
        'nameProduct': name,
        'stock': stock,
        'precio': precio
    })

    res.json(events);

}

const list = (req, res) => {

    const queryLength = Object.keys(req.query).length;
    const queryValue = Object.keys(req.query)[0];

    if ( queryLength === 0 ) {
        res.json(events);
    }  

    if ( queryValue !== 'nameProduct' && queryValue !== undefined){
        res.status(404).send();
        return;
    }

    if ( queryLength !== 0) {
        nameFilter = req.query['nameProduct'];
        filterName = events.filter( event => event.nameProduct === nameFilter);
        res.json(filterName);
    }

}

const change = (req, res) => {

    let idProduct = req.query.iD;
    id = parseInt(idProduct);


    if (idProduct === undefined) {
      res.status(400).send();
      return;
    }    

    if (req.body.nameProduct === undefined ||
        req.body.stock === undefined ||
        req.body.precio === undefined) {
        res.status(400).send();
        return;
    }

    let searchId = events.filter( event => event.id === id)

    if (searchId.length === 0) {
        res.status(404).send();
    }

    searchId.nameProduct = req.body.nameProduct
    searchId.stock = req.body.stock
    searchId.precio = req.body.precio

    res.send();

}

module.exports = {
    add,
    list,
    change
}