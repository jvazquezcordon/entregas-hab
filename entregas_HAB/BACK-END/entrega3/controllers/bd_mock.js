
const user = process.env.user;

const getUser = () => {
    return user;
}

module.exports = {
    getUser
}