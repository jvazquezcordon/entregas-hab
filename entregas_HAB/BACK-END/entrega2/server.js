/**
 * Un cliente nos pide realizar un sistema para gestionar eventos culturales.
 * Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o 
 * 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.
 * Opcionalmente pueden incluir una descripción.
 * 
 * El cliente necesitará una API REST para añadir conciertos y poder obtener
 * una lista de los existentes.
 * 
 * El objetivo del ejercicio es que traduzcas estos requisitos a una descripción
 * técnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles 
 * son los código de error a devolver
 * 
 * Notas:
 *    - el conocimiento necesario para realizarlo es el adquirido hasta la clase del
 *      miércoles
 *    - llega con un endpoint GET y otro POST
 *    - el almacenamiento será en memoria, por tanto cuando se cierre el servidor
 *      se perderán los datos. De momento es aceptable esto.
 * 
 */


const bodyParser = require('body-parser');
const express = require('express');
const app = express();

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", 'http://localhost:3000');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let eventos = {
  'conciertos': [],
  'teatros': [],
  'monologos': []
}

app.get('/eventos', (req, res) => {
  res.json(eventos);
})

app.post('/eventos/:collection', (req, res) => {

  const eventoParam = req.params.collection.toLowerCase();
  const eventoNombre = req.body.nombre;
  const eventoAforo = req.body.aforo;
  const eventoArtista = req.body.artista;
  //const eventoBody = req.body;

  const listaConciertos = eventos['conciertos'];

  if (['conciertos', 'teatros', 'monologos'].indexOf(eventoParam) === -1) {
    res.status(403).send();
    return;
  }

  if (eventos[eventoParam] === undefined) {
    res.status(404).send();
    return;
  }

  if (eventoNombre === undefined || eventoAforo === undefined || eventoArtista === undefined) {
    res.status(400).send();
    return;
  }

  const filter = listaConciertos
    .filter(concierto => concierto.nombre === eventoNombre)

  if ( filter.length !== 0 ) {
    res.status(409).send();
    return;
  }

  let data = {
    nombre: req.body.nombre,
    aforo: req.body.aforo,
    artista: req.body.artista,
    descripcion: req.body.descripcion
  }

  eventos[eventoParam].push(data);

  res.json(data);

})

const port = 3000;

app.listen(port);
