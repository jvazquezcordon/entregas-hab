/**
 * El objetivo de este ejercicio es implementar una aplicación de consola
 * para calcular la cuota mensual de un préstamo. Para ello, la aplicación debe:
 *     - usar una librería para realizar los cálculos necesarios (buscad en npmjs
 *       por las palabras loan, mortgage o payoff)
 *     - recibir por línea de comandos los argumentos necesarios para realizar los
 *       cálculos. Para esto podéis usar directamente la API de NodeJS (process.argv)
 *       o bien usar una librería como commander o yargs 
 *     - el resultado debe aparecer en la consola
 * 
 */


// --------------------------------------------------------------------------------------------------------
        
        
// *********//
// AMORTIZE //
// *********//
        
const amortize = require('amortize');

let amount = process.argv[2];
let rate = process.argv[3];
let totalTerm = process.argv[4];
let amortizeTerm = process.argv[5];
                
//console.log(process.argv)
                
const prestamo =  {
   amount,
   rate,
   totalTerm,
   amortizeTerm
};
                
let objCalculos = amortize(prestamo);
let objCalculosDesc = objCalculos.paymentRound;
                
console.log(objCalculos)
                
console.log(`Cuota a pagar: ${objCalculosDesc} €/mes`)


// --------------------------------------------------------------------------------------------------------
        
        
// ************//
//   LOAN-CALC //
// ***********//

/*
const loan_calc = require('loan-calc');
        
let amount = parseInt(process.argv[2]);
let rate = parseInt(process.argv[3]);
let termMonths = parseInt(process.argv[4]);
        
const prestamo = {
   amount,
   rate,
   termMonths
};
        
let cuota = loan_calc.paymentCalc(prestamo)
        
console.log(`Cuota a pagar: ${cuota} €/mes`)
*/