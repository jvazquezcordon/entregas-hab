/**
 * El objetivo de este ejercicio es implementar una aplicación de consola
 * para calcular la cuota mensual de un préstamo. Para ello, la aplicación debe:
 *     - usar una librería para realizar los cálculos necesarios (buscad en npmjs
 *       por las palabras loan, mortgage o payoff)
 *     - recibir por línea de comandos los argumentos necesarios para realizar los
 *       cálculos. Para esto podéis usar directamente la API de NodeJS (process.argv)
 *       o bien usar una librería como commander o yargs 
 *     - el resultado debe aparecer en la consola
 * 
 */



// ------------------------------------------------------------------------------------------------------


        // **********//
        //    LOAN   //
        // **********//


const loan = require('loan');

const op0 = process.argv[2];
const op1 = process.argv[3];
const op2 = process.argv[4];
const op3 = process.argv[5];
const op4 = process.argv[6];
const op5 = process.argv[7];

console.log(process.argv);

var prestamo = {
    type: op0,
    pay_every: op1,
    principal: op2,
    interest_rate: op3,
    invoice_fee: op4,
    instalments: op5
};

const objCalculos = loan(prestamo);

console.log(objCalculos);



// --------------------------------------------------------------------------------------------------------


        // *********//
        // AMORTIZE //
        // *********//


var amortize = require('amortize');

const op0 = process.argv[2];
const op1 = process.argv[3];
const op2 = process.argv[4];
const op3 = process.argv[5];

//console.log(process.argv)

const prestamo =  {
  amount: op0,
  rate: op1,
  totalTerm: op2,
  amortizeTerm: op3
};

const objCalculos = amortize(prestamo);
const objCalculosDesc = objCalculos.payment;

console.log(objCalculos)

console.log(`Cuota a pagar: ${objCalculosDesc.toFixed(2)} €/mes`)



// --------------------------------------------------------------------------------------------------------


        // ************//
        //   LOAN-CALC //
        // ***********//



var loan_calc = require('loan-calc');

const total = parseInt(process.argv[2]);
const interes = parseInt(process.argv[3]);
const tiempo = parseInt(process.argv[4]);

const prestamo = {
    amount: total,
    rate: interes,
    termMonths: tiempo
};

const cuota = loan_calc.paymentCalc(prestamo)

console.log(`Cuota a pagar: ${cuota} €/mes`)

