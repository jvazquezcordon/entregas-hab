import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/topartists',
    name: 'topartists',
    component: () => import('../views/TopArtists.vue')
  },
  {
    path: '/toptracks',
    name: 'toptracks',
    component: () => import('../views/TopTracks.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  },
  {
    path: '*',
    name: 'Error',
    component: () => import('../views/Error.vue')
  }

]

const router = new VueRouter({
  routes
})

export default router
