import config from './config.js'
const axios = require('axios').default

const apiKey = config.apiKey
const URL_BASE = `https://ws.audioscrobbler.com/`
const URL_GEO = `2.0/?
method=geo.gettopartists&country=spain&api_key=${apiKey}&format=json`

async function getArtists() {
    try {
        const response = await axios.get(`${URL_BASE}${URL_GEO}`)
        console.log(response)
        return response
    } catch (error) {
        console.log(error)
    }
}

const URL_GEO1 = `2.0/?
method=geo.getTopTracks&country=spain&api_key=${apiKey}&format=json`

async function getTopTracks() {
    try {
        const response = await axios.get(`${URL_BASE}${URL_GEO1}`)
        console.log(response)
        return response
    } catch (error) {
        console.log(error)
    }
}

const URL_GEO2 = `/2.0/?method=chart.gettoptags&api_key=${apiKey}&format=json`

async function getTopTags() {
    try {
        const response = await axios.get(`${URL_BASE}${URL_GEO2}`)
        console.log(response)
        return response
    } catch (error) {
        console.log(error)
    }
}

export default {
    getArtists,
    getTopTracks,
    getTopTags
}