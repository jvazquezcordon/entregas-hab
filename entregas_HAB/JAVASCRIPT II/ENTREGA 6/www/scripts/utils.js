// ELEMENTOS

let botones = document.getElementsByClassName('button')
    botones = Array.from(botones)

//const listadoLateral = document.querySelector("#listadoBarra")
const listado = document.querySelector("#listaBarra")



// FUNCIONES

const borrarLocalStorage = indice => {
    const etiquetas = cargarLocalStorage()
    etiquetas.splice(indice, 1)
    crearLista(etiqueta)
}

const eliminarCards = e => {

    if (e.target.classList.contains('boton_aside')){
        e.target.parentElement.remove()
    }

}


const crearInfo = etiquetas => {

    localStorage.setItem('etiqueta', JSON.stringify(etiquetas))

    const li = document.createElement('li')
    
    const hotel = {
        imagen: etiquetas.querySelector('div').querySelector('img').src,
        nombre: etiquetas.querySelector('h2').textContent,
        precio: etiquetas.querySelector('span').textContent
    }

    const imagenes = document.createElement('img')
          imagenes.classAtribute = 'src'
          imagenes.src = hotel.imagen

    li.appendChild(imagenes)

    const nombre = document.createElement('h2')
          nombre.textContent = hotel.nombre
    
    li.appendChild(nombre)

    const precio = document.createElement('p')
          precio.textContent = hotel.precio
    
    li.appendChild(precio)

    const button = document.createElement('button')
          button.textContent = 'Eliminar'
          button.classList = 'boton_aside'
          button.addEventListener('click', () => borrarLocalStorage(i))
          
    li.appendChild(button)
    
    listado.appendChild(li)

}

const cargarLocalStorage = () => {
    return JSON.parse(localStorage.getItem('etiquetas'))
}


const addLocalStorage = etiquetas => {
    const etiqueta = cargarLocalStorage() || {}
    const newMensajes = {...etiqueta, etiquetas}

    crearInfo(newMensajes)
}


const enviarInfo = e => {

    e.preventDefault(e)

    let article = e.target.parentElement
    let articleA = e.target.parentElement.parentElement

    
    if ( article.className == 'article'){
        crearInfo(article)
        addLocalStorage(article)
    } else {
        crearInfo(articleA)
        addLocalStorage(articleA)
    }
        
}


export { enviarInfo, botones, listado, eliminarCards, crearInfo, borrarLocalStorage }
