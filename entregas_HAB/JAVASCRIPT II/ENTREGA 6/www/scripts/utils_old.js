// ELEMENTOS

let botones = document.getElementsByClassName('button')
    botones = Array.from(botones)

//const listadoLateral = document.querySelector("#listadoBarra")
const listado = document.querySelector("#listaBarra")



// FUNCIONES

const eliminarCards = e => {

    if (e.target.classList.contains('boton_aside')){
        e.target.parentElement.remove()
    }

}


const crearInfo = etiquetas => {

    const li = document.createElement('li')
    
    const hotel = {
        imagen: etiquetas.querySelector('div').querySelector('img').src,
        nombre: etiquetas.querySelector('h2').textContent,
        precio: etiquetas.querySelector('span').textContent
    }

    const imagenes = document.createElement('img')
          imagenes.classAtribute = 'src'
          imagenes.src = hotel.imagen

    li.appendChild(imagenes)

    const nombre = document.createElement('h2')
          nombre.textContent = hotel.nombre
    
    li.appendChild(nombre)

    const precio = document.createElement('p')
          precio.textContent = hotel.precio
    
    li.appendChild(precio)

    const button = document.createElement('button')
          button.textContent = 'Eliminar'
          button.classList = 'boton_aside'
          
    li.appendChild(button)
    
    listado.appendChild(li)

}


const enviarInfo = e => {

    e.preventDefault(e)

    let article = e.target.parentElement
    let articleA = e.target.parentElement.parentElement

    
    if ( article.className == 'article'){
        crearInfo(article)
    } else {
        crearInfo(articleA)
    }
        
}


export { enviarInfo, botones, botonRemove, eliminarCards }