import { enviarInfo, botones, eliminarCards, listado, crearInfo, borrarLocalStorage } from './utils.js'


// LISTENERS

botones.forEach(boton => {

    boton.addEventListener('click', enviarInfo)
});

listado.addEventListener('click', eliminarCards);

document.addEventListener('DOMContentLoaded', crearInfo(JSON.parse(localStorage.getItem('etiquetas'))))
