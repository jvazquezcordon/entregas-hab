
/**
 * El objetivo del ejercicio es crear un nuevo array que contenga
 * todos los hashtags del array `tweets`, pero sin repetir
 * 
 * Nota: como mucho hay 2 hashtag en cada tweet
 */

tweets = [
    'aprendiendo #javascript en  #Vigo',
    'empezando el segundo #módulo del la lola #bootcamp',
    'hack a boss #bootcamp vigo #javascript codinglive'];

hastags = [];

hastagsFinal = [];

for (tweet of tweets) {

    busqueda1 = tweet.indexOf( "#" );

    divisor = tweet.slice( busqueda1, tweet.length );

    busqueda2 = divisor.indexOf("#");

    divisor2 = divisor.slice( 1, divisor.length );

    busqueda3 = divisor2.indexOf("#");

    divisor3 = divisor2.slice( busqueda3, divisor2.length );

    busqueda4 = divisor.indexOf(" ");

    divisor4 = divisor.slice( 0, busqueda4 );

    busqueda5 = divisor3.indexOf(" ");

    if ( divisor3.indexOf(" ") >= 0){

        divisor5 = divisor3.slice( 0, busqueda5 );

    }else{

        divisor5 = divisor3.slice( 0, divisor3.length );
    }


    hastags.push( divisor5, divisor4 );

}


    for (i=0; i<hastags.length; i++){

        if (hastagsFinal.indexOf(hastags[i]) == -1){

            hastagsFinal.push( hastags[i] );

        };

    }

    console.log(hastagsFinal);