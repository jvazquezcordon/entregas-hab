// https://jsonplaceholder.typicode.com/posts
// https://jsonplaceholder.typicode.com/posts/1

// a) Generar contador de mensajes por usuario
// b) Generar una lista con la siguiente estructura:

/*[
    {
        userId: <userId>,
        posts: [
            {
                title: <title>
                body: <body>     // hay que obtenerlo de la segunda petición
            },
            {
                title: <title>
                body: <body>
            },
        ]
    }
]
*/

const axios = require('axios');

const URL_WRITER = 'https://jsonplaceholder.typicode.com/posts/';

async function retrieveWriter() {

    const writers = await axios.get(URL_WRITER);
    const writersData = writers.data;

    let writersPromises = [];

    for (let write of writersData) {

        //console.log(write);
        //console.log(`${URL_WRITER}${write.id}`);
        const responseWriterDetail = await axios.get(`${URL_WRITER}${write.id}`)
        writersPromises.push(responseWriterDetail.data);
    }

    //console.log(writersPromises);

    const listOfWriters = Promise.all(writersPromises);

    //console.log(listOfWriters)
    return listOfWriters;

}

retrieveWriter()

    .then(resultados => {

        //console.log(writersData)
        //console.log(listOfWriters)

        /********************************************/
        // a) Generar contador de mensajes por usuario
        /********************************************/

        const objetoContadores = {};

        for (resultado of resultados) {

            if (objetoContadores[resultado.userId] === undefined) {
                objetoContadores[resultado.userId] = 1;

            } else {
                objetoContadores[resultado.userId]++;
            }

            //console.log(resultado);

        }

        //console.log(objetoContadores);

        /**************************************************/
        // b) Generar una lista con la siguiente estructura:
        /*************************************************/

        const objUserID = {};
        const arrUserID = [];

        for (result of resultados) {

            //console.log(result);
            //console.log(result.userId);


            if (objUserID[result.userId] === undefined) {

                objUserID[result.userId] =

                    {

                        userId: result.userId,
                        posts: [
                            {
                                title: result.title,
                                body: result.body
                            }
                        ]
                    };


            } else {

                objUserID[result.userId].posts.push({
                    title: result.title,
                    body: result.body
                });
            }

        }

        arrUserID.push(objUserID);

        console.log(arrUserID);

        return arrUserID;

    })