/**
 * Entregable semana 2
 * 
 * Escribe el código necesario para decidir en qué
 * fotografías sale Pablo. Como resultado se debe
 * obtener un array de strings con los nombres de las
 * fotografías.
 *  
 */

 const photos = [
    {
        name: 'Cumpleaños de 13',
        people: ['Maria', 'Pablo']  
    },
    {
        name: 'Fiesta en la playa',
        people: ['Pablo', 'Marcos']  
    },    
    {
        name: 'Graduación',
        people: ['Maria', 'Lorenzo']  
    },
 ]

 /*********************************************************************************************** */

 let arrayPhotos = [];

 let fotosPablo = photos.map( function (photo){

    for (let person of photo.people) {

        if ( person === "Pablo"){

            arrayPhotos.push(photo.name);
        }
    }

    return arrayPhotos;

});

console.log(arrayPhotos);