-- Ejercicio entregable SQL

-- cliente(#id, email, nombre, apellidos, direccion, contrasena)
-- reserva(#id, codigo, fecha, tiempo)
-- encargado(#id, email, nombre, apellidos, direccion, contrasena)
-- restaurantes(#id, nombre, direccion, capacidad)
-- asistentes(#id, -id_reserva, nombre, apellidos, dni)

USE reservas_Vigo;

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE usuario (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(50) UNIQUE NOT NULL,
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    direccion VARCHAR(50),
    contrasena VARCHAR(40) NOT NULL
);

CREATE TABLE reserva (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    codigo VARCHAR(10),
    fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    tiempo VARCHAR(10)
);

CREATE TABLE encargado (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(50) UNIQUE NOT NULL,
    nombre VARCHAR(50),
    apelllidos VARCHAR(50),
    direccion VARCHAR(50),
    contrasena VARCHAR(40) NOT NULL
);

CREATE TABLE restaurantes (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
    nombre VARCHAR(50), 
    direccion VARCHAR(50), 
    capacidad VARCHAR(4)
);

CREATE TABLE asistentes (
    id  INT UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
    nombre VARCHAR(50), 
    apellidos VARCHAR(50), 
    dni VARCHAR(9) UNIQUE,
    id_reserva INT UNSIGNED,
    FOREIGN KEY (id_reserva) REFERENCES reserva(id)
);

SET FOREIGN_KEY_CHECKS = 1;